@extends('app')

@section('content')
<div class="section no-pad-bot" id="index-banner">
    <div class="container">
        <br><br>
        <br><br>
        <br><br>
        <br><br>
        <h1 class="intro center red-text">Tasker</h1>
        <div class="row center">
            <h5 class="header col s12 light">Clean and transparent task managament</h5>
        </div>
        <div class="row center">
            <a href="/dashboard" id="download-button" class="btn-large waves-effect waves-light red">Get Started</a>
        </div>
        <br><br>
        <br><br>
        <br><br>
        <br><br>

    </div>
</div>
<div class="container">
    <div class="section">
      <!--   Icon Section   -->
      <div class="row">      

        <div class="col s12 m4">
            <div class="icon-block">
                <h2 class="center red-text"><i class="medium material-icons" >group</i></h2>
                <h5 class="center">Share with team</h5>

                <p class="light"></p>
            </div>
        </div>

        <div class="col s12 m4">
            <div class="icon-block">
                <h2 class="center red-text"><i class="medium material-icons">notifications</i></h2>
                <h5 class="center">Set up notifications</h5>

                <p class="light">
                </p>
            </div>
        </div>

        <div class="col s12 m4">
            <div class="icon-block">
                <h2 class="center red-text"><i class="medium material-icons">description</i></h2>
                <h5 class="center">Task activities</h5>

                <p class="light"></p>
            </div>
        </div>

      </div>

    </div>
    <br><br>

    <div class="section">
        
    </div>
</div>
@stop