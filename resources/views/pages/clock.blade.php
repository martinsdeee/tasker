@extends('clean')

@section('title', 'Pulkstenis')

@section('content')
<style>
	#clock {font-size: 108px}
	#hours {}
	#minutes {}
	.guide { font-size: 27px}
</style>

<div class="container ">

	<div class="row valign-wrapper cont">
		<div id="clock" class="col s12">

			<div class="row">
				<div class="card col s12 m8">
					<h1 id="hours" class="center-align">Stundas</h1>
				</div>
			</div>

			<div class="row">
				<div class="card col s12  m8 offset-m4">
					<h1 id="minutes" class="center-align">Minūtes</h1>
				</div>
			</div>

		</div>
	</div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
	(function(){

		var colors = {
			"-1": { classes: 'white-text red' },
			0: { classes: 'grey-text white' },
			1: { classes: 'white-text orange darken-4' },
			2: { classes: 'white-text light-blue' },
			3: { classes: 'white-text amber' },
			4: { classes: 'white-text blue-grey'},
			5: { classes: 'white-text green' },
			6: { classes: 'grey-text white' },
			7: { classes: 'white-text brown darken-4' },
			8: { classes: 'white-text blue darken-4' },
			9: { classes: 'white-text red' },
		}

		var	hours_in_words= {
			0: 'nulle nulle', 1: 'viens', 2: 'divi', 
			3: 'trīs', 4: 'četri', 5: 'pieci', 
			6: 'seši', 7: 'septiņi', 8: 'astoņi', 
			9: 'deviņi', 10: 'desmit', 11: 'vienpadsmit', 
			12: 'divpadsmit', 13: 'trīspadsmit', 14: 'četrpadsmit', 
			15: 'piecpadsmit', 16: 'sešpadsmit', 17: 'septiņpadsmit', 
			18: 'astoņpadsmit', 19: 'deviņpadsmit', 20: 'divdesmit', 
			21: 'divdesmit viens', 22: 'divdesmit divi', 23: 'divdesmit trīs', 
			24: 'divdesmit četri'	
		}

		var	minutes_in_words= {
			0: 'nulle nulle', 1: 'nulle viena', 2: 'nulle divas', 3: 'nulle trīs', 4: 'nulle četras', 5: 'nulle piecas', 
			6: 'nulle sešas', 7: 'nulle septiņas', 8: 'nulle astoņas', 
			9: 'nulle deviņas', 

			10: 'desmit', 11: 'vienpadsmit', 12: 'divpadsmit', 13: 'trīspadsmit', 14: 'četrpadsmit', 
			15: 'piecpadsmit', 16: 'sešpadsmit', 17: 'septiņpadsmit', 
			18: 'astoņpadsmit', 19: 'deviņpadsmit', 

			20: 'divdesmit', 21: 'divdesmit viena', 22: 'divdesmit divas', 23: 'divdesmit trīs', 
			24: 'divdesmit četras', 25: 'divdesmit piecas', 26: 'divdesmit sešas',	
			27: 'divdesmit septiņas', 28: 'divdesmit astoņas', 29: 'divdesmit deviņas',	

			30: 'trīsdesmit', 31: 'trīsdesmit viena', 32: 'trīsdesmit divas', 33: 'trīsdesmit trīs', 
			34: 'trīsdesmit četras', 35: 'trīsdesmit piecas', 36: 'trīsdesmit sešas',	
			37: 'trīsdesmit septiņas', 38: 'trīsdesmit astoņas', 39: 'trīsdesmit deviņas',

			40: 'četrdesmit', 41: 'četrdesmit viena', 42: 'četrdesmit divas', 43: 'četrdesmit trīs', 
			44: 'četrdesmit četras', 45: 'četrdesmit piecas', 46: 'četrdesmit sešas',	
			47: 'četrdesmit septiņas', 48: 'četrdesmit astoņas', 49: 'četrdesmit deviņas',

			50: 'piecdesmit', 51: 'piecdesmit viena', 52: 'piecdesmit divas', 53: 'piecdesmit trīs', 
			54: 'piecdesmit četras', 55: 'piecdesmit piecas', 56: 'piecdesmit sešas',	
			57: 'piecdesmit septiņas', 58: 'piecdesmit astoņas', 59: 'piecdesmit deviņas',

		}

		function reduce(x) {

			var result = 0;

			arr = x.toString().split("");

			for (var i = arr.length - 1; i >= 0; i--) {
				result = parseInt(result)+parseInt(arr[i]);				
			};

			if (result.toString().split("").length > 1) {
				return reduce(result);
			};
			
			return parseInt(result);
		}		

		

		function startTime() {
			var x = 3;
			var today=new Date();
			var h=today.getHours();
			var m=today.getMinutes();
			var s=today.getSeconds();
			//document.getElementById('clock').innerHTML = h+":"+m+":"+s;
			$('#hours').html(hours_in_words[h])
			.parent()
			.removeClass(colors[reduce(h)-1].classes)
			.addClass(colors[reduce(h)].classes); 
			$('#minutes').html(minutes_in_words[m])
			.parent()
			.removeClass(colors[reduce(m)-1].classes)
			.addClass(colors[reduce(m)].classes); 

			$('.cont').height($( document ).height()-100)
			console.log();

			var t = setTimeout(function(){startTime()},1000);
		}
		
		startTime();
	})()
</script>
@stop