## Tasker Application

This application will help manage routine tasks. Application build based on Laravel PHP Framework. 


### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
